<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('p1_id');
            $table->integer('p2_id');
            $table->integer('p1_scr')->nullable();
            $table->integer('p2_scr')->nullable();
            $table->integer('score')->nullable();
            $table->integer('team_id');
            $table->integer('match_id')->nullable();
            $table->integer('pair_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pairs');
    }
}
