<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColOversBowledPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('players', function (Blueprint $table) {
        $table->integer('overs_bowled')->default(0);
        $table->integer('bpick')->default(0);


     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players', function($table) {
         $table->dropColumn('overs_bowled');
         $table->dropColumn('bpick');
         
         
});
    }
}
