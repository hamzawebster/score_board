<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColBallsPlayed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('players', function (Blueprint $table) {
        $table->integer('balls_played')->default(0);
        $table->integer('four')->default(0);
        $table->integer('six')->default(0);
        $table->integer('strike_rate')->default(0);


     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players', function($table) {
         $table->dropColumn('balls_played');
         $table->dropColumn('four');
         $table->dropColumn('six');
         $table->dropColumn('strike_rate');
         
});

    }
}
