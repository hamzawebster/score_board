<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColBowledByOversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('overs', function (Blueprint $table) {
        $table->integer('bowled_by')->default(0);
        $table->integer('four')->default(0);
        $table->integer('six')->default(0);
        $table->integer('dot')->default(0);
        $table->integer('WD')->default(0);
        $table->integer('NB')->default(0);
        $table->integer('outs')->default(0);
        $table->dropColumn('out');

     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('overs', function($table) {
         $table->dropColumn('bowled_by');
         $table->dropColumn('six');
         $table->dropColumn('dot');
         $table->dropColumn('four');
         $table->dropColumn('NB');
         $table->dropColumn('WD');
        $table->dropColumn('outs');
         
});
    }
}
