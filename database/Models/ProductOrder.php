<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
* 
*/
class ProductOrder extends Model
{
	protected $table="productsorder";
	protected $fillable=['order_id','product_id','quantity'];
	public $timestamps=false;


	public function products(){
		return $this->hasMany('App\Models\Product','id','product_id');
	}
	public function order(){
		return $this->hasMany('App\Models\Order','id','order_id');
	}
}