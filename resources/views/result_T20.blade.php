
<html>
@include('includes.style_head2')

<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
@include('includes.navbar')
  
<br><br><br>

<div class="container">
  
  <div class="col-sm-6">
    <h2>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}}<b> V|S </b>
      {{App\teams::where('id',$_SESSION['teamB'])->first()->name}}</h2>
    <br>
  <a style="float:left;" class="btn btn-success" href="{{route('restart')}}">Restart Board</a>
  <br>
</div>
<br>
<center><h2 style="border-bottom:0.3px solid black"><strong>Match Summary</strong> </h2></center>
<br>


<div style="width:100%;float:left;">
  <h7><strong>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}}</strong> (1st Innings)</h7>
  <br>
  
  <table style="background-color:#fbf7f7;" class="table table-hover">
    <thead><b>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}} Batting</b></thead>
  <tr>
    
    <th>BATSMEN</th>
    
    <th>R</th>
    <th>B</th>
    <th>4s</th>
    <th>6s</th>
    <th>SR</th>

  </tr>
  
  @foreach($Aplayer as $ap)
  <tr>
    <td>{{$ap->name}}</td>
    
    <td>{{$ap->score}}</td>
    <td>{{$ap->balls_played}}</td>
    <td>{{$ap->four}}</td>
    <td>{{$ap->six}}</td>
    <td>{{$ap->strike_rate}}</td>
</tr>

  @endforeach
  <tr>
    <th>Extras</th>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>{{$_SESSION['Aextra']}} Runs</td>
  </tr>
  <tr>
    <th>Total</th>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>
      <strong>
    {{$_SESSION['Atotal']}}/{{$_SESSION['Aout_sum']}}(20 overs,RR:{{round($_SESSION['Arun_rate'],2)}})
  </strong>
  </td>
  </tr>
</table>

</div>


<br><br>
<div style="width:100%;float:left;">
  <center>
  <h3>{{App\teams::where('id',$_SESSION['teamB'])->first()->name}} Bowling Statistics</h3>    
  </center>
  <table class="table table-hover">
  <tr>
    <th>Bowlers</th>
    <th>O</th>
    
    <th>R</th>
    <th>W</th>
    <th>Econ</th>
    <th>0s</th>
    <th>4s</th>
    <th>6s</th>
    <th>NB</th>
    <th>WD</th>

  </tr>
  @foreach($Abowlers as $ap)
  <tr>
    <td>{{App\players::where('id',$ap->bowled_by)->first()->name}}</td>

    <td>{{App\overs::where('match_id',$_SESSION['match_id'])
      ->where('team_id',$_SESSION['teamA'])
      ->where('bowled_by',$ap->bowled_by)
      ->get()
      ->count()
        }}</td>
    
    <td>
      {{json_decode(App\overs::where('team_Id',$_SESSION['teamA'])
            ->where('bowled_by',$ap->bowled_by)      
            ->selectRaw('sum(score) as score')      
            ->groupBy('bowled_by')->first())->score}}
    </td>
    <td>
      {{$ap->outs}}
    </td>
    <td>
      RR:{{round((json_decode(App\overs::where('team_Id',$_SESSION['teamA'])
            ->where('bowled_by',$ap->bowled_by)      
            ->selectRaw('sum(score) as score')      
            ->groupBy('bowled_by')->first())->score)/App\overs::where('match_id',$_SESSION['match_id'])
      ->where('team_id',$_SESSION['teamA'])
      ->where('bowled_by',$ap->bowled_by)
      ->get()
      ->count(),2)}}

    </td>
    <td>
      {{$ap->dots}}
    </td>
    <td>
      {{$ap->fours}}
    </td>
    <td>
      {{$ap->sixs}}
    </td>
    <td>
      {{$ap->NB}}
    </td>
    <td>
      {{$ap->WD}}
    </td>
  </tr>
  @endforeach
</table>
</div>

<br>

<div style="width:100%;float:left;">
  <h7><strong>{{App\teams::where('id',$_SESSION['teamB'])->first()->name}}'s Innings</strong> (20 overs maximum)</h7>
  <table style="background-color:#fbf7f7;" class="table table-hover">
  <tr>
    
    <th>BATSMEN</th>
    
    <th>R</th>
    <th>B</th>
    <th>4s</th>
    <th>6s</th>
    <th>SR</th>

  </tr>
  
  @foreach($Bplayer as $ap)
  <tr>
    <td>{{$ap->name}}</td>
    
    <td>{{$ap->score}}</td>
    <td>{{$ap->balls_played}}</td>
    <td>{{$ap->four}}</td>
    <td>{{$ap->six}}</td>
    <td>{{$ap->strike_rate}}</td>
</tr>

  @endforeach
  <tr>
    <th>Extras</th>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>{{$_SESSION['Bextra']}} Runs</td>
  </tr>
  <tr>
    <th>Total</th>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>
      <strong>
    {{$_SESSION['Btotal']}}/{{$_SESSION['Bout_sum']}} (RR:{{round($_SESSION['Brun_rate'],2)}})
  </strong>
  </td>
  </tr>
</table>
</div>
<br>
<div style="width:100%;float:left;">
  <center>
  <h3>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}} Bowling Statistics</h3>    
  </center>
  <table class="table table-hover">
  <tr>
    <th>Bowlers</th>
    <th>O</th>
    
    <th>R</th>
    <th>W</th>
    <th>Econ</th>
    <th>0s</th>
    <th>4s</th>
    <th>6s</th>
    <th>NB</th>
    <th>WD</th>

  </tr>
  @foreach($Bbowlers as $ap)
  <tr>
    <td>{{App\players::where('id',$ap->bowled_by)->first()->name}}</td>

    <td>{{App\overs::where('match_id',$_SESSION['match_id'])
      ->where('team_id',$_SESSION['teamB'])
      ->where('bowled_by',$ap->bowled_by)
      ->get()
      ->count()
        }}</td>
    
    <td>
      {{json_decode(App\overs::where('team_Id',$_SESSION['teamB'])
            ->where('bowled_by',$ap->bowled_by)      
            ->selectRaw('sum(score) as score')      
            ->groupBy('bowled_by')->first())->score}}
    </td>
    <td>
      {{$ap->outs}}
    </td>
    <td>
      RR:{{round((json_decode(App\overs::where('team_Id',$_SESSION['teamB'])
            ->where('bowled_by',$ap->bowled_by)      
            ->selectRaw('sum(score) as score')      
            ->groupBy('bowled_by')->first())->score)/App\overs::where('match_id',$_SESSION['match_id'])
      ->where('team_id',$_SESSION['teamB'])
      ->where('bowled_by',$ap->bowled_by)
      ->get()
      ->count(),2)}}
      
    </td>
    <td>
      {{$ap->dots}}
    </td>
    <td>
      {{$ap->fours}}
    </td>
    <td>
      {{$ap->sixs}}
    </td>
    <td>
      {{$ap->NB}}
    </td>
    <td>
      {{$ap->WD}}
    </td>
  </tr>
  @endforeach
</table>
</div>


 
</div>
  
</body>
</html>


<!-- how it works starts here -->

