
<html>
@include('includes.style_head2')
<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
@include('includes.navbar')
  
<br><br><br>
<center>
  <div  class="container">
  <h2>Welcome to the Score Board </h2>

<br>

<form method="post" action="{{route('save_teams')}}">
          {{csrf_field()}}
          <div class="col-sm-12">
            
              <div class="col-sm-4 form-group">
                <label><h3><strong>Team A</strong></h3></label>
                <input required type="text" placeholder="Enter First Team Name here.." class="form-control" name="teamA">
              </div>
              <div class="col-sm-4">
                <center>
                  <strong><b>V/S</b></strong>
                </center>
                
              </div>
              
              <div class="col-sm-4 form-group">
              <label><h3><strong>Team B</strong></h3></label>
                <input required type="text" placeholder="Enter Second Team Name here.." class="form-control" name="teamB">
              </div>
            
             <div class="col-sm-4 form-group">
              <label><h3><strong>Match Type</strong></h3></label>
                
                <select name="match_type" class="form-control">   @foreach($config as $c)           
                  <option>{{$c->match_type}}</option>
                @endforeach  
                              
                                    
                </select>
              </div>
                    

          <div class="col-sm-4 form-group">
              
              
              <button title="Select details about match" style="width:100%" type="Submit" class="btn btn-info">Go to Match Details</button>
            </div>          
          </div>
        </form>
  
</div>
</center>

  <!-- <script type="text/javascript">
    $('#cb').click(function(){
      alert('haha');
    });
  </script> -->
</body>
</html>


<!-- how it works starts here -->

