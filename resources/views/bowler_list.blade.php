
<html>
@include('includes.style_head2')

<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
@include('includes.navbar')
  
<br><br><br>
<center>
  <div class="container">
  <h2>Select Bowler </h2>

<br>

<form method="post" action="{{route('set_bowler')}}">
          {{csrf_field()}}
          <div class="col-sm-12">
            
            <div class="col-sm-4 form-group">
              <label><h3><strong>Bowlers</strong></h3></label>
                <select  name="bowler" class="form-control">
                  @foreach($baller as $b)
                  <option value="{{$b->id}}">{{$b->name}}</option>
                  @endforeach
                </select>
              </div>
                    

          <div class="col-sm-4 form-group">
              
              @if($_SESSION['total_overs']==$_SESSION['over_counter'])
              <button title="Start match with selected bowler"  style="width:100%" type="Submit" class="btn btn-info">Start Match</button>
              @else
              <button title="Continue match with selected bowler" style="width:100%" type="Submit" class="btn btn-info">Continue Match</button>
              @endif
            </div>          
          </div>
        </form>
  
</div>

</center>
  
</body>
</html>


<!-- how it works starts here -->

