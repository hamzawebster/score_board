
<html>
@include('includes.style_head2')

<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
@include('includes.navbar')
 
<br><br><br>

 <center>
  <div class="container">
  <h2>Select Pair to Bat from <strong>{{App\teams::where('id',$_SESSION['bat_first'])->first()->name}}</strong> </h2>

  <br>
  <form id="myform" method="post" action="{{route('save_pair')}}">
    {{csrf_field()}}


    <div class="col-sm-12">

     <div class="col-sm-6 form-group">
      <label>First Player</label>

      <select id="pl1" name="p1" class="form-control">  @foreach($batsmen as $b)            
        <option  value="{{$b->id}}">{{$b->name}}</option>
        @endforeach             

      </select>
    </div> 

    <div class="col-sm-6 form-group">
      <label>Second Player</label>
      <h6 style="display:none;color:red" id="eror">*Kindly Select two different players before starting match
      </h6>
      <select id="pl2" name="p2" class="form-control">  @foreach($batsmen->reverse() as $b)            
        <option  value="{{$b->id}}">{{$b->name}}</option>
        @endforeach             

      </select>
    </div> 




  </div>
  
</form> 
</div>
</center>

<center>
  <div class="container">
  <div class="col-sm-12">
    <div class="col-sm-6 form-group">
      <button title="Select Bowler from list of bowlers" style="width:100%" id="mybtn" class="btn btn-info">Select Bowler</button>
    </div>
        <div class="col-sm-6 form-group">

      <a title="Back to Select Teams" style="width:100%" class="btn btn-danger" href="{{route('restart')}}">Restart</a>

    </div>
  </div>
</div>
</center>

<script type="text/javascript">
  var a = document.getElementById("pl1");
  var b = document.getElementById("pl2");
  var x = document.getElementById("eror");

  $("#mybtn").on("click", function(){
  //console.log('in if cond');
  if (a.options[a.selectedIndex].value == b.options[b.selectedIndex].value) {

    x.style.display = 'block';
  } 
  else 
  {
    $( "#myform" ).submit();
  }
});
    </script>

    </body>
</html>


<!-- how it works starts here -->

