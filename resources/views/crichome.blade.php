
<html>
<head>
  @include('includes.style_head')
</head>
<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
  <nav style="" class="navbar fixed-top navbar navbar-expand-md bg-dark navbar-dark ">
    <a style="" class="navbar-brand" href="#">The Score Board</a>
    

    <div  class="collapse navbar-collapse" id="collapsibleNavbar">
    <!-- <ul class="navbar-nav">
      <li class="nav-item">
        <a id="homelink" class="nav-link" href="">Home</a>
      </li>
      <li class="nav-item">
        <a id="howlink" class="nav-link" href="#">How it works?</a>
      </li>
      <li class="nav-item">
        <a id="keylink" class="nav-link" href="#">Key features</a>
      </li>
      <li class="nav-item">
        <a id="testimonialslink" class="nav-link" href="#">Testimonials</a>
      </li>
       <li class="nav-item">
        <a id="registrationlink" class="nav-link" href="#">Register Now</a>
      </li>     
    </ul> -->
  </div>  
</nav>


<div style="margin-top:-35px;" class="container">

  <center>    
    <h3 style="font-family: 'Roboto Slab', serif;">Team {{App\teams::where('id',$_SESSION['bat_first'])->first()->name}}    
    </h3>


  </center>
  
  <div id="small-table">
    <center>

      <div class="panel-group">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">

             <table>
               <tr>
                <td><h5> <strong>{{App\teams::where('id',$_SESSION['bat_first'])->first()->name}}: <strong>{{$score_sum}}
                  @if($_SESSION['match_type']=='T20')
                  -{{$out_sum}}
                  @endif
                </strong></strong></h5></td>
                <td><h5 style="margin-left:20px"><strong> Overs</strong>:{{$over_NO}}.{{$ball_NO-1}} </h5></td>
                <td>&nbsp&nbsp&nbsp&nbsp</td>
                <td>
                  <a id="viewmore" style="color:black;" class="btn btn-xs btn-default" data-toggle="collapse" href="#collapse1">View More <span class="fa fa-arrow-down"></span></a>
                  <a id="viewless" style="display:none; color:black;" class="btn btn-xs btn-default" data-toggle="collapse" href="#collapse1">View Less <span class="fa fa-arrow-up"></span></a>
                </td>
              </tr>
            </table>
          </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse">
          <div class="panel-body">
            <table>
              <tr>
                <td><h5> <strong>{{App\teams::where('id',$_SESSION['bat_first'])->first()->name}}: <strong>{{$score_sum}}
                  @if($_SESSION['match_type']=='T20')
                  -{{$out_sum}}
                  @endif
                </strong></strong></h5></td>
                <td><h5 style="margin-left:20px"><strong> Overs</strong>:{{$over_NO}}.{{$ball_NO-1}} </h5></td>
              </tr>
              <tr>
                <td>
                  <h5><strong>{{App\players::where('id',$_SESSION['p1'])->first()->name}}@if($_SESSION['face_by']==$_SESSION['p1'])
                    *
                    @endif:   
                  </strong>  {{App\players::where('id',$_SESSION['p1'])->first()->score}} runs 
                  <b>&nbsp s/r:</b> ({{App\players::where('id',$_SESSION['p1'])->first()->strike_rate}})
                </h5>

              </td>
              <td>
                <h5 style="margin-left:20px"><strong>{{App\players::where('id',$_SESSION['p2'])->first()->name}}
                  @if($_SESSION['face_by']==$_SESSION['p2'])
                  *
                @endif:   </strong>  {{App\players::where('id',$_SESSION['p2'])->first()->score}} runs
                <b>&nbsp s/r:</b>  ({{App\players::where('id',$_SESSION['p2'])->first()->strike_rate}})
              </h5> 
            </td>
          </tr>
          <tr>
            <td>
              <h5 ><strong>Run Rate:</strong>  {{round($run_rate, 2)}} rpo</h5> 
            </td>
            <td>
              <h5 style="margin-left:20px;" ><strong>Extras:</strong>  {{$extra_sum}}  </h5> 
            </td>

          </tr>
          <tr>

          </tr>
        </table>
      </div>
      @if(isset($_SESSION['inning']))
      <div  class="panel-footer">          
        <h5>
          <b>
            Target by {{App\teams::where('id',$_SESSION['teamA'])->first()->name}}: {{$_SESSION['Atotal']}}
          </b>

        </h5>         
      </div>
      @endif
    </div>
  </div>
</div>



</center>

</div>



</div>

<!-- ///////////////This is A SAFE POINT///////// -->

<div id="main" class="container">

  <form method="post" action="{{route('update')}}">
    {{csrf_field()}}

    <!-- ///////////////////////////////// -->
    
    
      <div class="row">
        <div class="col-sm-4">
       <div style="height:150px;" class="well" >

         <h4 style="font-family: 'Roboto Slab', serif;">Extras:</h4>


         <center>
           <div class="btn-group" data-toggle="buttons">


            <input value="ok" type="hidden" name="validity" id="option2" autocomplete="off" checked="checked">


            <label title="Wide Ball" id="margin-left-5px" class="btn btn-primary">
              WD
              <input value="WB"  type="radio" name="validity" id="option1" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>

            <label title="No Ball" id="margin-left-5px" class="btn btn-info">
              NB
              <input value="NB" type="radio" name="validity" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>

            <label title="Leg Bye" style="width:65px;" id="margin-left-5px" class="btn btn-default">
              LB
              <input value="LB" type="radio" name="validity" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>

            <label title="Bye score" id="margin-left-5px" class="btn btn-warning">
              BYE
              <input value="LB" type="radio" name="validity" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>

          </div>
        </center>

      </div>
    </div>
    <div  class="col-sm-4">
      <div style="height:150px;" class="well" >

        <h4 style="font-family: 'Roboto Slab', serif;">Out Status:</h4>
        
        
        <center>
          <div class="btn-group" data-toggle="buttons">      

            <label title="Not Out" id="margin-left-5px" style="width:129px" class="btn btn-default active">
              NO
              <input value="0" type="radio" name="out" id="option2" autocomplete="off" checked="checked">
              <span class="glyphicon glyphicon-ok"></span>
            </label>

            <label title="Got Out " id="margin-left-5px" style="width:129px" class="btn btn-warning">
              Yes
              <input value="1" type="radio" name="out" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>


          </div>
        </center>

      </div>
    </div>
    <div  class="col-sm-4">
      <div  class="well" >

        <strong><b><h4 style="font-family: 'Roboto Slab', serif;">Score:</h4></b></strong>
        
        
        <center>
        
          <div class="btn-group" data-toggle="buttons">

            <input style="display:none;" value="0" type="radio" name="score" id="option2" autocomplete="off" checked="checked">

            <label id="score-btn" class="btn btn-default">
              0
              <input onchange="this.form.submit();" value="0" type="radio" name="score" id="option1" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>


            <label id="score-btn" class="btn btn-warning">
              1
              <input onchange="this.form.submit();" value="1" type="radio" name="score" id="option1" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>

            <label id="score-btn" class="btn btn-primary">
              2
              <input onchange="this.form.submit();" value="2" type="radio" name="score" id="option1" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>


            <label id="score-btn" class="btn btn-info">
              3
              <input onchange="this.form.submit();" value="3" type="radio" name="score" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>

            <label id="score-btn" class="btn btn-default">
              4
              <input onchange="this.form.submit();" value="4" type="radio" name="score" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>
            <br>
            <label id="score-btn1" class="btn btn-warning">
              5
              <input onchange="this.form.submit();" value="5" type="radio" name="score" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>


            <label id="score-btn1" class="btn btn-info">
              6
              <input onchange="this.form.submit();" value="6" type="radio" name="score" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>
            <label id="score-btn1" class="btn btn-danger">
              7
              <input onchange="this.form.submit();" value="7" type="radio" name="score" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>
            <label id="score-btn1" class="btn btn-default">
              8
              <input id="margin-left-5px" onchange="this.form.submit();" value="8" type="radio" name="score" id="option2" autocomplete="off">
              <span class="glyphicon glyphicon-ok"></span>
            </label>

          </div>
        </center>

      </div>
    </div>
    </div>

    <center>
    <div class="row">
      <div class="col-sm-12 form-group">               
        <!-- <label class="form-control">Over NO {{$over_NO}}</label> -->
        <input class="form-control" type="hidden" value="{{$over_NO}}"  name="over_id">      
        <!-- <label class="form-control">Ball NO {{$ball_NO}}</label> -->
        <input class="form-control" value="{{$ball_NO}}" type="hidden"  name="ball_NO">

        <input class="form-control" value="{{$_SESSION['face_by']}}" type="hidden"  name="face_by">


          <!-- <div class="row">
            <div class="col-sm-4 form-group">
              <label>Extra</label>
              
            </div>

            <div class="col-sm-4 form-group">

              <label >Out</label>
              <select class="form-control" name="out">
                <option value="0">no</option>
                <option value="1">yes</option>

              </select>
            </div>

            <div class="col-sm-4 form-group">
             <label >Score</label>
             <select onchange="this.form.submit()" class="form-control" name="score">
              <option value="0">score on this ball</option>
               <option>0</option>
               <option>1</option>
               <option>2</option>
               <option>3</option>
               <option>4</option>
               <option>5</option>
               <option>6</option>

             </select>             
           </div>

         </div> -->
       </div>
       
       <br>
       <div class="container">
          


          <div class="col-sm-2">

            <button title="Update Score-Board with&#013; selected options" style="margin-top:10px" style="width:100%" class="btn btn-primary form-group form-control">Update Score <span  class="fa fa-arrow-right" aria-hidden="true"></span></button>


          </div>

          <div title="Refresh Score-board to its&#013; default settings" style="margin-top:10px" class="col-sm-2">
            <a onclick="window.location.reload()" style="width:100%" href="#" class="btn btn-info form-group">Refresh Board <span class="fa fa-refresh"></span></a>

          </div>
          

          @if($ball_NO-1 > 0 and $_SESSION['undo-triger']==0 and $_SESSION['match_type']!='T20')
           <div title="Ball again and undo last ball" style="margin-top:10px" class="col-sm-2">
            <a style="width:100%" class="btn btn-warning form-group" href="{{route('undo')}}">Undo Last Ball <span class="fa fa-undo"></span> </a>       
          </div> 
          @endif
          <div title="Quit to the main menu" style="margin-top:10px" class="col-sm-2">
            <a style="width:100%" class="btn btn-danger form-group" href="{{route('restart')}}">Quit <span class="fa fa-power-off"></span></a>       
          </div>

        
        
        
      </div>
    </div>
  </center>
  

</form>


</div>

</div>
</div>
</div>
<!-- footer start -->
<br><br>
<script type="text/javascript">
  $('#viewmore').click(function(){
    
    console.log('viewmore clicked');
    $('#viewmore').hide();
    $('#viewless').show();

    $.cookie('viewmore', true);
});
    $('#viewless').click(function(){
    
    console.log('viewless clicked');
    $('#viewmore').show();
    $('#viewless').hide();

    $.cookie('viewmore', false);
});
  if($.cookie('viewmore') == 'true'){
    $('#viewmore').click();
} 

</script>

</body>
</html>


<!-- how it works starts here -->

