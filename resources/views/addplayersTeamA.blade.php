
<html>
@include('includes.style_head2')

<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
@include('includes.navbar')
  
<br><br><br>
<center>
  <div class="container">
  <h2>Add Players ({{$teamAname}})</h2>

<br>
 <form method="post" action="{{route('addplayers_teamA')}}">
          {{csrf_field()}}
            <input type="hidden" value="{{$no_players}}" name="no_players">
            <input type="hidden" value="{{$teamBid}}" name="teamBid">
            <input type="hidden" value="{{$teamBname}}" name="teamBname">
            <!-- looping according to required No_players from 1 to n-->

              @for($i=1;$i<=$no_players;$i++)
          <input type="hidden" value="{{$teamAid}}" name="teamAid">
          
            <div class="col-sm-12">
          
              <div class="col-sm-6 form-group">
                
                <label>Player No. {{$i}}</label>
                <input required type="text" placeholder="Enter Name here.." class="form-control" name="p{{$i}}">
              
              </div>
             
            
                
            </div>
          
          
            @endfor()

            <!-- looping according to required No_players ends -->
            
          
            
         <div class="col-sm-6 form-group">
              
              
              <button title="Enter Player names for team {{$teamBname}}" style="width:100%" type="Submit" class="btn btn-info">Add Players for {{$teamBname}}</button>
            </div>   
            <div class="col-sm-6 form-group">
              
              <a title="Back to Select Teams" style="width:100%" class="btn btn-danger" href="{{route('restart')}}">Restart
              </a>
              
            </div>

          </div>
        </form> 
  
</div>

</center>
  
</body>
</html>


<!-- how it works starts here -->

