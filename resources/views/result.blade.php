
<html>
@include('includes.style_head2')

<body>
  <!-- navbar start -->
    
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
@include('includes.navbar')
  
<br><br><br>

<div class="container">
  
  <div class="col-sm-6">
    <h2 >Innings Over</h2>
    <br>
  <a style="float:left;" class="btn btn-success" href="{{route('restart')}}">Restart Board</a>
  <br><br>
</div>
<br>
<center><h2 style="border-bottom:0.3px solid black"><strong>Match Summary</strong> </h2></center>
<br>
<br>
<div style="width:50%;float:left;">
  
  <table class="table table-hover">
  <tr>
    
    <th>Pair No.</th>
    <th>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}}'s Score</th>
    <th><center>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}} Batsmen</center></th>
  </tr>
  <?php 
    $i=1;
   ?>
  @foreach($Apairs as $ap)
  <tr>
    <td>{{$ap->pair_no}}</td>
    <td>{{$ap->score}}</td>
    <td>
      <center>{{App\players::where('id',$ap->p1_id)->first()->name}}</center>
      
      <center>&</center><center>{{App\players::where('id',$ap->p2_id)->first()->name}}</center></td>
    <?php 
      $score[$i]=$ap->score;
      $i++;
     ?>
  </tr>
  @endforeach
</table>
</div>


<div style="width:50%;float:left;">
 
  <table  class="table table-hover">
  <tr>
    
    <th>{{App\teams::where('id',$_SESSION['teamB'])->first()->name}}'s Score</th>
    <th><center>{{App\teams::where('id',$_SESSION['teamB'])->first()->name}} Batsmen</center></th>
    <th>Point Goes To</th>
  </tr>
  <?php $j=1;
  $_SESSION['teamA_points']=0;
  $_SESSION['teamB_points']=0;
   ?>
  @foreach($Bpairs as $bp)
  <tr>   
 
    <td>{{$bp->score}}</td>
    <td>
      <center>{{App\players::where('id',$bp->p1_id)->first()->name}}</center>
      
      <center>&</center><center>{{App\players::where('id',$bp->p2_id)->first()->name}}</center></td>

    @if($score[$j]>$bp->score)
    <td>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}}</td>
    <?php $_SESSION['teamA_points']++; ?>

    @elseif($score[$j]<$bp->score)
    <td>{{App\teams::where('id',$_SESSION['teamB'])->first()->name}}</td>
    <?php $_SESSION['teamB_points']++; ?>
    @else
    <td>Draw</td>    
    @endif

  </tr>
  <?php $j++ ?>
  @endforeach
  
</table>
</div>

<div style="width:100%;float:left;">
  <center>
  <h3>{{App\teams::where('id',$_SESSION['teamB'])->first()->name}} Bowlinging Statistics</h3>    
  </center>
  <table style="background-color:#f0f7f6;" class="table table-hover">
  <tr>
    <th>Bowlers</th>
    <th>O</th>
    
    <th>R</th>
    <th>W</th>
    <th>Econ</th>
    <th>0s</th>
    <th>4s</th>
    <th>6s</th>
    <th>NB</th>
    <th>WD</th>

  </tr>
  @foreach($Abowlers as $ap)
  <tr>
    <td>{{App\players::where('id',$ap->bowled_by)->first()->name}}</td>

    <td>{{App\overs::where('match_id',$_SESSION['match_id'])
      ->where('team_id',$_SESSION['teamA'])
      ->where('bowled_by',$ap->bowled_by)
      ->get()
      ->count()
        }}</td>
    
    <td>
      {{json_decode(App\overs::where('team_Id',$_SESSION['teamA'])
            ->where('bowled_by',$ap->bowled_by)      
            ->selectRaw('sum(score) as score')      
            ->groupBy('bowled_by')->first())->score}}
    </td>
    <td>
      {{$ap->outs}}
    </td>
    <td>
      {{round((json_decode(App\overs::where('team_Id',$_SESSION['teamA'])
            ->where('bowled_by',$ap->bowled_by)      
            ->selectRaw('sum(score) as score')      
            ->groupBy('bowled_by')->first())->score)/App\overs::where('match_id',$_SESSION['match_id'])
      ->where('team_id',$_SESSION['teamA'])
      ->where('bowled_by',$ap->bowled_by)
      ->get()
      ->count(),2)}}
      
    </td>
    <td>
      {{$ap->dots}}
    </td>
    <td>
      {{$ap->fours}}
    </td>
    <td>
      {{$ap->sixs}}
    </td>
    <td>
      {{$ap->NB}}
    </td>
    <td>
      {{$ap->WD}}
    </td>
  </tr>
  @endforeach
</table>
</div>
<br>
<br>
<br>
<div style="width:45%;float:left;">
  <center>
  <h3>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}} Batsmen</h3>    
  </center>
  <table class="table table-hover">
  <tr>
    <th>Player</th>
    <th>Score</th>
    <th>Out</th>

  </tr>
  @foreach($Aplayer as $ap)
  <tr>
    <td>{{$ap->name}}</td>
    <td>{{$ap->score}}</td>
    <td>0{{$ap->out}} times</td>

  </tr>
  @endforeach
</table>
</div>
<div style="width:45%;float:right;">
 <center>
    <h3>{{App\teams::where('id',$_SESSION['teamB'])->first()->name}} Batsmen</h3>
 </center>

  <table class="table table-hover">
  <tr>
    <th>Player</th>
    <th>Score</th>
    <th>Out</th>

  </tr>
  @foreach($Bplayer as $bp)
  <tr>
    <td>{{$bp->name}}</td>
    <td>{{$bp->score}}</td>
    <td>0{{$bp->out}} times</td>


  </tr>
  @endforeach
</table>
</div>
<br>
<div style="width:100%;float:left;">
  <center>
  <h3>{{App\teams::where('id',$_SESSION['teamA'])->first()->name}} Bowling Statistics</h3>    
  </center>
<table style="background-color:#f0f7f6;" class="table table-hover">
  <tr>
    <th>Bowlers</th>
    <th>O</th>
    
    <th>R</th>
    <th>W</th>
    <th>Econ</th>
    <th>0s</th>
    <th>4s</th>
    <th>6s</th>
    <th>NB</th>
    <th>WD</th>

  </tr>
  @foreach($Bbowlers as $ap)
  <tr>
    <td>{{App\players::where('id',$ap->bowled_by)->first()->name}}</td>

    <td>{{App\overs::where('match_id',$_SESSION['match_id'])
      ->where('team_id',$_SESSION['teamA'])
      ->where('bowled_by',$ap->bowled_by)
      ->get()
      ->count()
        }}</td>
    
    <td>
      {{json_decode(App\overs::where('team_Id',$_SESSION['teamB'])
            ->where('bowled_by',$ap->bowled_by)      
            ->selectRaw('sum(score) as score')      
            ->groupBy('bowled_by')->first())->score}}
    </td>
    <td>
      {{$ap->outs}}
    </td>
    
      <td>
       {{round((json_decode(App\overs::where('team_Id',$_SESSION['teamB'])
            ->where('bowled_by',$ap->bowled_by)      
            ->selectRaw('sum(score) as score')      
            ->groupBy('bowled_by')->first())->score)/App\overs::where('match_id',$_SESSION['match_id'])
      ->where('team_id',$_SESSION['teamB'])
      ->where('bowled_by',$ap->bowled_by)
      ->get()
      ->count(),2)}}
      
    </td>
    
    <td>
      {{$ap->dots}}
    </td>
    <td>
      {{$ap->fours}}
    </td>
    <td>
      {{$ap->sixs}}
    </td>
    <td>
      {{$ap->NB}}
    </td>
    <td>
      {{$ap->WD}}
    </td>
  </tr>
  @endforeach
</table>
</div>



<div style="width:45%;float:left" class="jumbotron">
 <center>
    <h2 style="border-bottom:.1px solid black"><strong>Team {{App\teams::where('id',$_SESSION['teamA'])->first()->name}}</strong></h2>
 </center>
   <h2><b> Total Runs:</b> {{$_SESSION['Atotal']}}
   </h2>
   <h2><b>Pair Points: </b> {{$_SESSION['teamA_points']}}</h2>
   <h2> 
    <b>Run Rate</b>: {{round($_SESSION['Arun_rate'],2)}} rpo
  </h2>
  <h2>
    <b>Extras:</b> {{$_SESSION['Aextra']}}

  </h2>

</div>
<div style="width:45%;float:right" class="jumbotron">
 <center>
    <h2 style="border-bottom:.1px solid black"><strong>Team {{App\teams::where('id',$_SESSION['teamB'])->first()->name}}</strong></h2>
 </center>
  <h2><b>Total Runs:</b> {{$_SESSION['Btotal']}} 
  </h2>
   <h2>
    <b>Pair Points: </b> {{$_SESSION['teamB_points']}}
  </h2>


  <h2><b>Run Rate</b>: {{round($_SESSION['Brun_rate'],2)}} rpo
  </h2>
    <h2><b>Extras:</b> {{$_SESSION['Bextra']}}
  </h2>

  

</div>
<br>
 
</div>
  
</body>
</html>


<!-- how it works starts here -->

