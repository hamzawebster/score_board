
<html>
@include('includes.style_head2')

<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
@include('includes.navbar')
  
<br><br><br>

<div class="container">
  <h2>Select next batsman <strong>{{App\teams::where('id',$_SESSION['bat_first'])->first()->name}}</strong> </h2>

  <br>
  <form id="myform" method="post" action="{{route('set_new_player')}}">
    {{csrf_field()}}


    <div class="col-sm-12">

     <div class="col-sm-6 form-group">
      <label>Next Batsman</label>

      <select required id="next_player" name="next_player" class="form-control"> 
          <option value="" disabled selected>Select Next Batsman</option> 
       @foreach($batsmen as $b)
                 
        <option  value="{{$b->id}}">{{$b->name}}</option>
        @endforeach             

      </select>

    </div> 

    <div class="col-sm-6 form-group">
      <label>Who gets out?</label>

      <select required id="gets_out" name="gets_out" class="form-control">
        <option value="" disabled selected>Which Player got out?</option>            
        <option id="gets_outo2" value="{{$_SESSION['p2']}}">{{App\players::where('id',$_SESSION['p2'])->first()->name}}</option>
        <option id="gets_outo1"  value="{{$_SESSION['p1']}}">{{App\players::where('id',$_SESSION['p1'])->first()->name}}</option>

      </select>
      
    </div> 

    <div class="col-sm-6 form-group">
      <label>Who shall face?</label>

      <select required id="face" class="form-control"> 
         <option value="" disabled selected>Select the player on strike</option>
         <option id="face1"></option>
         <option id="face2"></option>
      </select>
      
    </div>
    
  </div>
  
 
</div>
</div>
<div class="container">
  <div class="col-sm-12">
    <div class="col-sm-6 form-group">
      <button style="width:100%" id="mybtn" class="btn btn-info">Continue</button>
    </div>
    <div class="col-sm-6 form-group">

      <a style="width:100%" class="btn btn-danger" href="{{route('restart')}}">Quit Match</a>

    </div>
  </div>
</div>

</div>

</form>
<script type="text/javascript">
  
  var p1=<?php echo $_SESSION['p1'];  ?>;
  var p2=<?php echo $_SESSION['p2']; ?>;  
  var gets_outo1=$("#gets_outo1").val();
  var gets_outo2=$("#gets_outo2").val();

  //console.log(face2val+"abc");
  
  $("#gets_out").change(function () {
    
    if($('#gets_out').val()==p1)
    {

      var face1val=p2;
      <?php $_SESSION['face1val']=$_SESSION['p2'];  ?>
      var gets_outtext=$("#gets_outo2").html();
      console.log(gets_outtext);

    }
    else
    {
      console.log('in p2 cond');
      var face1val=p1;
      <?php $_SESSION['face1val']=$_SESSION['p1']; 
      
       ?>
       var gets_outtext=$("#gets_outo1").html();
      console.log(gets_outtext);

    }
        $('#face1').html(gets_outtext).val(face1val);
      
  });

</script>
<script type="text/javascript">
    $("#next_player").on("change", function(){
    var face2val=$("#next_player").val();
  $('#face2').html($("#next_player :selected").text()).val(face2val)
      ;
});
</script>
    
    </body>
</html>


<!-- how it works starts here -->

