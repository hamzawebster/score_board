
<html>

@include('includes.style_head2')


<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
@include('includes.navbar')
  
<br><br><br>
<center>
  <div class="container">
  <h2>Match Details </h2>

<br>
 <form method="post" action="{{route('save_match')}}">
          {{csrf_field()}}
          <input type="hidden" value="{{$teamA}}" name="teamA">
          <input type="hidden" value="{{$teamB}}" name="teamB">

              <input type="hidden" value="{{$teamAname}}" name="teamAname">
          <input type="hidden" value="{{$teamBname}}" name="teamBname">

          <div class="col-sm-12">
           
              <div class="col-sm-6 form-group">
                <label class="pull-left">Match Name</label>
                <input required type="text" placeholder="Enter Match Name here.." value="{{$teamAname}} v/s {{$teamBname}}" class="form-control" name="name">
              </div>
          
           
              
              <div class="col-sm-6 form-group">
                <label>Batting First</label>
                <select name="bat_first" class="form-control">              
                  <option value="{{$teamA}}">{{$teamAname}}</option>
                  <option value="{{$teamB}}">{{$teamBname}}</option>              
                                    
                </select>
              </div>

            
              @if($_SESSION['match_type']=='Indoor Classic')
            <div class="col-sm-6 form-group">
                <label>Players and overs</label>
                <select name="no_players" class="form-control">              
                  <option value="2">2 players - 3 overs</option>
                  <option value="4">4 players - 6 overs</option>
                  <option value="6">6 players - 9 overs</option>
                  <option value="8">8 players - 12 overs</option>
                 <option value="10">10 players - 15 overs</option>             
                </select>
              </div> 
              @elseif($_SESSION['match_type']=='Indoor Advance')
               <div class="col-sm-6 form-group">
                <label>Players and overs</label>
                <select name="no_players" class="form-control">              
                  <option value="2">2 players -4 overs</option>
                  <option value="4">4 players -6 overs</option>
                  <option value="6">6 players -8 overs</option>
                  <option value="8">8 players -10  overs</option>
                  <option value="10">10 players -12  overs</option>             
                </select>
              </div>
              

              @endif   
            

         <div class="col-sm-6 form-group">
          
              <button title="Enter Player names for team {{$teamAname}}" style="width:100%" type="Submit" class="btn btn-info">Add Players for {{$teamAname}}</button>
            </div>   
            <div class="col-sm-6 form-group">
              
              <a title="Back to Select Teams" style="width:100%" class="btn btn-danger" href="{{route('restart')}}">Back</a>
              
            </div>        
          </div>
        </form> 
  
</div>
</center>


  <!-- <script type="text/javascript">
    $("#no_players").on("change", function(){
    var total_overs=document.getElementById('total_overs');
    var overs_per_pair=document.getElementById('overs_per_pair');
    var no_players=document.getElementById('no_players');
    
    var x=$("#total_overs").value=((no_players.value/2)*overs_per_pair.value);
    $("#total_overs").val(x);
    console.log(x);
    });
    $("#overs_per_pair").on("change", function(){
    var total_overs=document.getElementById('total_overs');
    var overs_per_pair=document.getElementById('overs_per_pair');
    var no_players=document.getElementById('no_players');
    
    var x=$("#total_overs").value=((no_players.value/2)*overs_per_pair.value);
    $("#total_overs").val(x);
    console.log(x);
    }); 
  </script> -->
</body>
</html>


<!-- how it works starts here -->

