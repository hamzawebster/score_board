
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.6/css/intlTelInput.css">
  <link rel="stylesheet" href="/css/intlTelInput.css">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="https://fonts.googleapis.com/css?family=Indie+Flower|Roboto+Slab|Shadows+Into+Light" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('/css/radio.css')}}">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>


  <script src="/js/intlTelInput.min.js"></script>

  <title>Score Board</title>
</head>
<body>
  <!-- navbar start -->
  
  <!-- A grey horizontal navbar that becomes vertical on small screens -->
  <nav style="" class="navbar fixed-top navbar navbar-expand-md bg-dark navbar-dark ">
    <a class="navbar-brand" href="#">The Score Board</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div  class="collapse navbar-collapse" id="collapsibleNavbar">
    <!-- <ul class="navbar-nav">
      <li class="nav-item">
        <a id="homelink" class="nav-link" href="">Home</a>
      </li>
      <li class="nav-item">
        <a id="howlink" class="nav-link" href="#">How it works?</a>
      </li>
      <li class="nav-item">
        <a id="keylink" class="nav-link" href="#">Key features</a>
      </li>
      <li class="nav-item">
        <a id="testimonialslink" class="nav-link" href="#">Testimonials</a>
      </li>
       <li class="nav-item">
        <a id="registrationlink" class="nav-link" href="#">Register Now</a>
      </li>     
  </ul> -->
</div>  
</nav>
<br><br><br>

<div class="container">
  <h2>Innings Over</h2>
<br><br>
  <h2>Score: <strong>{{$score_sum}}-{{$out_sum}}</strong></h2>
  <a href="{{route('crichome')}}" class="btn btn-lg btn-info">Click here to start second inings</a>
</div>
  
</body>
</html>


<!-- how it works starts here -->

