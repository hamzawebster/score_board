<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
* 
*/
class Order extends Model
{
	// protected $table="orders";
	protected $fillable=['user_id','billing_address','shipping_address','
	amount','order_date','status'];
	public $timestamps=false;


	/*public function productOrder(){
		return $this->belongsTo('App\Models\ProductOrder','id','order_id');
	}*/
}