<?php

namespace App\Http\Controllers;
// use App\Models\Product;
use App\Models\Order;
use App\Models\ProductOrder;
class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewOrder($orderId){
        return response()->json(Order::where('id',$orderId)->get());
    }

    public function productsInOrder($orderId){
        $order=Order::findOrFail($orderId);
        $products=ProductOrder::where('order_id',$order->id)->first()->products;
        return response()->json($products);
    }

    public function addOrder(Request $request){
        $inputs=$request->all();
        if(Order::create($inputs)){
            return response()->json(['status'=>'ok']);
        }else{
            return response()->json(['status'=>'fails']);
        }

    }
}
