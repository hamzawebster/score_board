<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Cart;
use App\Models\Order;
use App\Models\ProductOrder;
use Auth;
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // product listing
    public function index()
    {

        $products=Product::all();
        $categories=Category::all();
        return view('/UserViews/products',compact('products','categories'));
    }
    public function search(Request $request)
    {
        $categories=Category::all();
        $products=Product::where('name','LIKE','%'.($request->q).'%')->get();
        return view('/UserViews/products',compact('products','categories'));
    }
    public function add_to_cart(Request $request)
    {
        $products=Product::all();
        $categories=Category::all();
        $x=Cart::add($request->id,$request->name,$request->qty,$request->price);
        

                 
        
        return view('/UserViews/products',compact('products','categories'));
    }
    public function view_cart()
    {
        $products=Product::all();
        $categories=Category::all();
        $cart=Cart::content();
        $total=Cart::subtotal();
        

                 
        
        return view('/UserViews/checkout',compact('products','categories','cart'));
    }
    public function delete($rowId)
    {
        $products=Product::all();
        $categories=Category::all();
        $cart=Cart::content();
        Cart::remove($rowId);
        //dd($cart);

                 
        
        return view('/UserViews/checkout',compact('products','categories','cart'));
    }
    public function orderdetail()
    {
        
        return view('/UserViews/orderdetails');
    }

     public function checkout(Request $request)
    {
        $content=Cart::content();
        $products=Product::all();
        $categories=Category::all();
        $total=Cart::subtotal();
        // dd($total);

        $order=new Order();
        $order->user_id=Auth::user()->id;
        $order->amount=$total;
        $order->shipping_address=$request->address;
        $order->order_date=$request->date;
           
        $order->save();
       // echo $order->id;
       // echo $content->id;
       // die();
        foreach ($content as $c) {
        ProductOrder::create([
            'order_id'=>$order->id,
            'product_id'=>$c->id,
            'quantity'=>$c->qty,
            ''
        ]);    


        }
        Cart::destroy();
        return redirect()->route('products')->with('success','Your Order has been Successfully Submitted! Thanks for shopping');

        }
        
    
    }

