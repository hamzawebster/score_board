<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;
use App\Models\Product;
class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // list of categories
    public function index(){
        $categories= Category::all();
        return view('/UserViews/category',compact('categories'));
    }
    public function getproducts($id)
    {
        $products=Product::where('category_id',$id)->get();
        $categories= Category::all();
        return view('../UserViews/products',compact('products','categories'));
    }

    
}
