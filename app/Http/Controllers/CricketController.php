<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\teams;
use App\match;
use App\players;
use App\pairs;
use App\balls;
use App\overs;
use App\configuration;


class CricketController extends Controller

{
	public function getinfo()
	{
		$config=configuration::all();

		return view('getinfo',compact('config'));
	}

	public function save_teams(Request $request)
	{
		session_start();
		session_destroy();
		//saving team A
		$team= new teams();
		$team->name=$request->teamA;
		$team->save();
		//saving teamA ends

		$teamA=$team->id;
		//saving teamB		
		$teamb= new teams();
		$teamb->name=$request->teamB;
		$teamb->save();
		//saving teamB
		session_start();
		$_SESSION['match_type']=$request->match_type;
		$teamB=$teamb->id;
		$teamAname=$request->teamA;
		$teamBname=$request->teamB;
	///////SESSIONS TO BE USED IN 'function update' FUNCTION FOR NEW OVER/////
		$_SESSION['wd']=0;
		$_SESSION['nb']=0;
		$_SESSION['six']=0;			
		$_SESSION['four']=0;
		$_SESSION['dot']=0;
		$_SESSION['over_outs']=0;
		$_SESSION['over_score']=0;
		$_SESSION['last_ball_out']=0;
	//////////////////////////////////////////////////////				
		return view('matchinfo',compact('teamA','teamB','teamAname','teamBname'));
	}
	public function save_match(Request $request)
	{
		session_start();
		//Configuration table has settings for differnt match formats e.g overs per pair or score awarded on extras
		$config=configuration::where('match_type',$_SESSION['match_type'])->first();
		$overs_per_pair=$config->overs_per_pair;

	//saving Match settings
		$m = new match();
		$m->name= $request->name;
		$m->teamAid=$request->teamA;
		$m->teamBid=$request->teamB;
		$m->batting_first=$request->bat_first;
		if($_SESSION['match_type']=='T20')
		{
			//11 players nd 20 overs by default in T20 format
			$m->no_players=11;		
			$m->no_overs=20;
			//jaba
			
		}
		else
		{
			
			$m->no_players=$request->no_players;	
			//NO of overs according to the number of players,here $overs_per_pair is extracted from $config
			$m->no_overs=(($request->no_players)/2)*($overs_per_pair);
		}

		$m->save();
		///Match settings save end

		///Which team is not batting shall be saved in $_SESSION['bteam']
		if($m->batting_first==$request->teamA)
		{
			$_SESSION['bteam']=$request->teamB;
		}
		else if($m->batting_first==$request->teamB)
		{
			$_SESSION['bteam']=$request->teamA;
		}
		///Which team is not batting shall be saved in $_SESSION['bteam'] end

		
		$no_players=$request->no_players;
		$teamBname=$request->teamBname;
		$teamAname=$request->teamAname;
		$teamAid=$request->teamA;
		$teamBid=$request->teamB;  

		$_SESSION['match_id']=$m->id;
		$_SESSION['teamA']=$teamAid;
		$_SESSION['teamB']=$teamBid;

		$_SESSION['total_overs']=$m->no_overs;
		$_SESSION['total_balls']=0;
		$_SESSION['over_counter']=$_SESSION['total_overs'];
		//first batting team's total score
		$_SESSION['Atotal']=0;
		//second batting team's total score
		$_SESSION['Btotal']=0;
		//by default pair NO is 1 and is also incremented where needed 
		$_SESSION['pair_no']=1;
		//As we are using a previous valid ball for further calculations so this row is stored before starting
		$ball = new balls();
		$ball->over_id=0;
		$ball->ball_NO=0;
		$ball->face_by=0;
		$ball->ball_by=0;
		$ball->validity='ok';
		$ball->score=0;
		$ball->out=0;
		$ball->match_id=$_SESSION['match_id'];
		$ball->save();
		if($_SESSION['match_type']=='T20')
		{
			$no_players=11; //jaba
		}
		$_SESSION['teamAname']=$teamAname;
		$_SESSION['teamBname']=$teamBname;

		return view('addplayersTeamA',compact('no_players','teamBname','teamAname','teamBid','teamAid'));    	
	}

	public function addplayers_teamA(Request $request)
	{
		session_start();
		$teamBname=$request->teamBname;
		$teamBid=$request->teamBid;
		$no_players=$request->no_players;
		$teamAname=$request->teamBname;
		$teamAid=$request->teamBid;

    	//using no_players to loop according to requirement
		for($i=1;$i<=$request->no_players;$i++)
		{
			//name of input field from where data is comming is dynamic due to loop like p1 p2 so both view's loop and this loop are in a sync
			$pname='p'.$i;
			///////////////////	
			$player= new players();
			$player->name=$request->$pname;
			$player->team_id=$request->teamAid;
			$player->match_id=$_SESSION['match_id'];
			$player->save();
		}

		return view('addplayersTeamB',compact('teamBid','teamBname','no_players','teamAid','teamAname'));
	}

	public function addplayers_teamB(Request $request)
	{
		//same procedure explained for addplayers_teamA
		session_start();
		$teamBname=$request->teamBname;
		$teamBid=$request->teamBid;
		$teamAname=$request->teamAname;
		$teamAid=$request->teamAid;
    	//dd($teamBname);
		for($i=1;$i<=$request->no_players;$i++)
		{
			$pname='p'.$i;	
			$player= new players();
			$player->name=$request->$pname;
			$player->team_id=$request->teamBid;
			$player->team_id=$request->teamAid;
			$player->match_id=$_SESSION['match_id'];
			$player->save();
		}

		$bat_first=match::where('id',$_SESSION['match_id'])->select('batting_first')->first();
		$bat_first=$bat_first->batting_first;
		$batsmen=players::where('team_id',$bat_first)
		->where('pick',0)
		->get();
		
		$_SESSION['bat_first']=$bat_first;    	
		return view('getpair',compact('batsmen','teamBname','teamBid','teamAid','teamAname'));
	}

	public function save_pair(Request $request)
	{
		session_start();
		//config has by default settings saved according to match type
		$config=configuration::where('match_type',$_SESSION['match_type'])->first();
		//Saving Pair
		$pair= new pairs();
		$pair->p1_id=$request->p1;
		$pair->p2_id=$request->p2;
		//$_SESSION['bat_first'] always has id of batting team
		$pair->team_id=$_SESSION['bat_first'];
		$pair->pair_no=$_SESSION['pair_no'];
		$pair->match_id=$_SESSION['match_id'];
		$pair->save();
		//saving Pair end

		//$changepick act as a flag which shows which players have already came to bat
		$changepick=players::where('id',$request->p1)
		->first();
		$changepick->pick=1;
		$changepick->save();
		$changepick=players::where('id',$request->p2)
		->first();
		$changepick->pick=1;
		$changepick->save();
		////Change Pick status Ends

		//saving sessions for players of a pair who came to bat
		$_SESSION['p1']=$pair->p1_id;
		$_SESSION['p2']=$pair->p2_id;
		//by default p1 shall be facing initially
		$_SESSION['face_by']=$_SESSION['p1'];
		$_SESSION['pair']=$pair->id;
		
		//Assigning pair id to p1
		$editplayer1=players::where('id',$pair->p1_id)->first();
		$editplayer1->pair_id=$pair->id;
		$editplayer1->save();
		//Assigning pair id to p1 ends

		//Assigning pair id to p2		
		$editplayer2=players::where('id',$pair->p2_id)->first();
		$editplayer2->pair_id=$pair->id;
		$editplayer2->save();
		//Assigning pair id to p2 ends

		//Getting players of balling team '$_SESSION['bteam']' to pass in the next view where bowler shall be selected 
		$baller=players::where('overs_bowled','<',$config->overs_limit)
		->where('team_Id',$_SESSION['bteam'])
		->where('match_id',$_SESSION['match_id'])
			//bpick flag ensures that last over isnt bowled by the same bowler
		->where('bpick',0)
		->get();

		return view('bowler_list',compact('baller'));
	}

	Public function set_bowler(Request $request)
	{
		session_start();

		$_SESSION['current_baller']=$request->bowler;
		//All other player's bpick shall be 0 except the $now_baller
		$bpick_zero=players::where('team_Id',$_SESSION['bteam'])->update([
			'bpick'=>0
		]);
		$now_baller=players::where('id',$request->bowler)
		->first();
		$now_baller->overs_bowled++;
		$now_baller->bpick=1;
		$now_baller->save();
		return redirect()->route('crichome');
	}


	public function ball() 
	{

		session_start();

		$config=configuration::where('match_type',$_SESSION['match_type'])->first();

		if(!isset($_SESSION['face_by']))
		{
			$_SESSION['face_by']=$_SESSION['p1'];	
		}	
		//just declaration 
		$out_sum=0;
		$score_sum=0;
		$extra_sum=0;
		
		//$_SESSION['bat_first'] always has batting team's id in it
		$score=players::where('team_id',$_SESSION['bat_first'])
		->where('match_id',$_SESSION['match_id'])
		->get();


		//calculating sum of score,extras nd outs
		foreach($score as $b)
			{	$score=$b->score;
				$score_sum=$score_sum+$score;
				$extra=$b->extra;				
				$extra_sum=$extra_sum+$extra;				
				$out=$b->out;
				$out_sum=$out_sum+$out;
			}
			$score_sum=$score_sum+$extra_sum;			
		//calculating sum of score,extras nd outs ends			
		//$last_valid_ball is the row with valid ball bowled
			if($_SESSION['match_type']=='Indoor Advance')
			{
				//considering wide and no ball as a ball according to rules
				$last_valid_ball=balls::where('match_id',$_SESSION['match_id'])
				->orderBy('id','desc')->take(1)->first();
			}
			else
			{
				//selecting row with last valid ball bowled
				$last_valid_ball=balls::where('match_id',$_SESSION['match_id'])
				->where('validity','ok')
				->orWhere('validity','=','BY')
				->orWhere('validity','=','LB')
				->orderBy('id','desc')->take(1)->first();

			}

			//getting over number from last valid
			$over_NO=$last_valid_ball->over_id;


			////////////calculating runrate////////////
			try
			{
				$run_rate=($score_sum*6)/$_SESSION['total_balls'];				
			}
			catch(\Exception $ex){
				$run_rate=0;
			}
			////////////////////run rate ends////////////////


			//////Calculating Strike Rate //////

			//$_SESSION['face_by'] has player who faced ball
			$onstrike=players::where('id',$_SESSION['face_by'])
			->first();
			try
			{
				$strikerate=($onstrike->score*100)/($onstrike->balls_played);
			}
			catch(\Exception $ex)
			{
				$strikerate=0;
			}
			$onstrike->strike_rate=$strikerate;
			$onstrike->save();
			/////////strike rate ends//////////////

			$ball_NO=$last_valid_ball->ball_NO+1; 
			
				////over  increment///

			$overfinish=0;

			if($ball_NO > 6)
			{	
		//overfinish is a flag to select new bowler
				$overfinish=1;
				$ball_NO=1;
				$over_NO=$over_NO+1;
		//$_SESSION['over_counter'] initially is equal to total overs so it decrements with over finish		
				$_SESSION['over_counter']--;

			}
				////over increment ends////
			if($_SESSION['match_type']!="T20")
			{

				if($over_NO > $config->overs_per_pair)
				{	
					$ball = new balls();
					$ball->over_id=0;
					$ball->ball_NO=0;
					$ball->face_by=0;
					$ball->ball_by=0;
					$ball->validity='ok';
					$ball->score=0;
					$ball->out=0;
					$ball->match_id=$_SESSION['match_id'];

					$ball->save();
					$pair=pairs::where('match_id',$_SESSION['match_id'])
					->where('team_id',$_SESSION['bat_first'])
					->where('pair_no',$_SESSION['pair_no'])->first();
					$p1_scr=players::where('id',$_SESSION['p1'])->first()->score;
					$p2_scr=players::where('id',$_SESSION['p2'])->first()->score;
					$p1_extra=players::where('id',$_SESSION['p1'])->first()->extra;
					$p2_extra=players::where('id',$_SESSION['p2'])->first()->extra;
					$pair->p1_scr=$p1_scr;
					$pair->p2_scr=$p2_scr;
					$pair->score=$p1_scr+$p2_scr+$p1_extra+$p2_extra;
					$pair->save();
					$_SESSION['pair_no']++;



					if($_SESSION['over_counter']<0)
					{

						$_SESSION['over_counter']=$_SESSION['total_overs'];	
						if(isset($_SESSION['inning']))
						{
							$_SESSION['Brun_rate']=$run_rate;
							$_SESSION['Btotal']=$score_sum;
							$_SESSION['Bextra']=$extra_sum;
						//$_SESSION['run_rate']=$run_rate;
							return redirect()->route('result');
						}
						$_SESSION['pair_no']=1;
						$_SESSION['Atotal']=$score_sum;
						$_SESSION['Aextra']=$extra_sum;
						
						$temp=$_SESSION['bat_first'];
						$_SESSION['bat_first']=$_SESSION['bteam'];
						$_SESSION['bteam']=$temp;
						$_SESSION['Arun_rate']=$run_rate;
						$_SESSION['inning']=1;
						$_SESSION['total_balls']=0;
					}
					$batsmen=players::where('team_id',$_SESSION['bat_first'])
					->where('pick',0)
					->get();

					return view('getpair',compact('batsmen'));

				}
					//dd($ball_NO);

				if($overfinish==1)
				{
					$overfinish=0;
					$baller=players::where('overs_bowled','<',$config->overs_limit)
					->where('team_Id',$_SESSION['bteam'])
					->where('match_id',$_SESSION['match_id'])
					->where('bpick',0)
					->get();
					$ball = new balls();
					$ball->over_id=$over_NO;
					$ball->ball_NO=0;
					$ball->face_by=0;
					$ball->ball_by=0;
					$ball->validity='ok';
					$ball->score=0;
					$ball->out=0;
					$ball->match_id=$_SESSION['match_id'];

					$ball->save();
					return view('bowler_list',compact('baller'));
				}
				

			}
			else
			{

				//jaba
				if($out_sum > 9)
				{

					$over= new overs();
			//$over->id=$request->over_id;

					$over->team_id=$_SESSION['bat_first'];
					$over->pair_id=$_SESSION['pair'];
					$over->match_id=$_SESSION['match_id'];
					$over->score=$_SESSION['over_score'];
					$over->outs=$_SESSION['over_outs'];
					$over->bowled_by=$_SESSION['current_baller'];
					$over->six=$_SESSION['six'];
					$over->four=$_SESSION['four'];
					$over->dot=$_SESSION['dot'];
					$over->WD=$_SESSION['wd'];
					$over->NB=$_SESSION['nb'];
					$over->save();
					$_SESSION['over_outs']=0;
					$_SESSION['over_score']=0;
					$_SESSION['wd']=0;
					$_SESSION['nb']=0;
					$_SESSION['six']=0;			
					$_SESSION['four']=0;
					$_SESSION['dot']=0;
				}
				if($_SESSION['over_counter']==0 or $out_sum > 9 )
				{
					//dd("here");
					$ball = new balls();
					$ball->over_id=0;
					$ball->ball_NO=0;
					$ball->face_by=0;
					$ball->ball_by=0;
					$ball->validity='ok';
					$ball->score=0;
					$ball->out=0;
					$ball->match_id=$_SESSION['match_id'];

					$ball->save();
					//dd('hi');
					$_SESSION['over_counter']=$_SESSION['total_overs'];	
					if(isset($_SESSION['inning']))
					{
						$_SESSION['Brun_rate']=$run_rate;
						$_SESSION['Btotal']=$score_sum;
						$_SESSION['Bextra']=$extra_sum;
						$_SESSION['Bout_sum']=$out_sum;

						//$_SESSION['run_rate']=$run_rate;
						return redirect()->route('result_T20');

					}
					$_SESSION['pair_no']=1;
					$_SESSION['Atotal']=$score_sum;
					$_SESSION['Aextra']=$extra_sum;
					$_SESSION['Aout_sum']=$out_sum;
					$temp=$_SESSION['bat_first'];
					$_SESSION['bat_first']=$_SESSION['bteam'];
					$_SESSION['bteam']=$temp;
					$_SESSION['Arun_rate']=$run_rate;
					$_SESSION['inning']=1;

					$_SESSION['total_balls']=0;
					$batsmen=players::where('team_id',$_SESSION['bat_first'])
					->where('pick',0)
					->get();

					return view('getpair',compact('batsmen'));
				}
				if($overfinish==1)
				{
					$overfinish=0;
					$baller=players::where('overs_bowled','<',$config->overs_limit)
					->where('team_Id',$_SESSION['bteam'])
					->where('match_id',$_SESSION['match_id'])
					->where('bpick',0)
					->get();
					$ball = new balls();
					$ball->over_id=$over_NO;
					$ball->ball_NO=0;
					$ball->face_by=0;
					$ball->ball_by=0;
					$ball->validity='ok';
					$ball->score=0;
					$ball->out=0;
					$ball->match_id=$_SESSION['match_id'];

					$ball->save();
					return view('bowler_list',compact('baller'));
				}
				if(isset($_SESSION['inning']) and $_SESSION['Atotal'] < $score_sum)
				{
					$_SESSION['Brun_rate']=$run_rate;
					$_SESSION['Btotal']=$score_sum;
					$_SESSION['Bextra']=$extra_sum;
					$_SESSION['Bout_sum']=$out_sum;

						//$_SESSION['run_rate']=$run_rate;
					return redirect()->route('result_T20');

				}

				
			}

			//dd($score_sum);
			return view('crichome',compact('over_NO','ball_NO','out_sum','score_sum','run_rate','extra_sum','strikerate'));
		}

		public function update(Request $request)
		{
			session_start();
			$_SESSION['last_face_by']=$_SESSION['face_by'];
			$_SESSION['undo-triger']=0;
			$_SESSION['last_played_by']=$request->face_by;
			$_SESSION['last_score']=$request->score;
			unset($_SESSION['last-wd-score']);
			unset($_SESSION['last-nb-score']);


			if($request->out==1)
			{
				$_SESSION['over_outs']++;

			}
			
			$_SESSION['over_score']=$request->score+$_SESSION['over_score'];
			if($request->validity=='WB')
			{
				$_SESSION['wd']++;

			}
			elseif ($request->validity=='NB') {
				$_SESSION['nb']++;				
			}
			if($request->score==6)
			{
				$_SESSION['six']++;
			}
			elseif ($request->score==4) {
				$_SESSION['four']++;

			}
			elseif ($request->score==0) {
				$_SESSION['dot']++;
				
			}

			$config=configuration::where('match_type',$_SESSION['match_type'])->first();

			$ball= new balls();

			$ball->over_id=$request->over_id;
			$ball->score=$request->score;
			$ball->out=$request->out;

			$ball->validity=$request->validity;
			$ball->face_by=$request->face_by;
			$ball->match_id=$_SESSION['match_id'];
			$ball->team_id=$_SESSION['bat_first'];


			if($request->validity=='ok')
			{
				//updatig player score o a valid ball
				$_SESSION['total_balls']++;
				$playerscore=players::where('id',$_SESSION['face_by'])->first();

				$playerscore->balls_played++;
				if($request->score=='4')
				{
					$playerscore->four++;
				}
				if($request->score=='6')
				{
					$playerscore->six++;
				}
				$initialscore=$playerscore->score;
				$playerscore->score=($initialscore)+($request->score);

				

				if($request->out==1)
				{
					$playerscore->score=($playerscore->score)-($config->out_deduct);				

					$playerscore->out++;
					$_SESSION['last_ball_out']=1;
				}
				$playerscore->save();
///////////////////////////////////////////
				if($request->ball_NO==6)
				{   
					$ball->ball_NO=$request->ball_NO;
					$ball->over_id=$request->over_id;
					$ball->save();
			///saving whole over score
					$score_sum=0;
					$out_sum=0;

					$scr=balls::where('over_id',$request->over_id)->get();
					foreach($scr as $s)
					{
						$out=$s->out;
						$out_sum=$out_sum+$out;
						$score=$s->score;
						$score_sum=$score_sum+$score;
					}
			///changing facing batsmen ..	
					if(($ball->score)%2 != 0)
					{

						if($_SESSION['face_by']==$_SESSION['p2'])
						{
							$_SESSION['face_by']=$_SESSION['p2'];
						}
						else
						{
							$_SESSION['face_by']=$_SESSION['p1'];

						}

					}
					else{
						if($_SESSION['face_by']==$_SESSION['p2'])
						{
							$_SESSION['face_by']=$_SESSION['p1'];
						}
						else
						{
							$_SESSION['face_by']=$_SESSION['p2'];

						}
					}

		/// changing facing batsmen end

					$over= new overs();
			//$over->id=$request->over_id;

					$over->team_id=$_SESSION['bat_first'];
					$over->pair_id=$_SESSION['pair'];
					$over->match_id=$_SESSION['match_id'];
					$over->score=$_SESSION['over_score'];
					$over->outs=$_SESSION['over_outs'];
					$over->bowled_by=$_SESSION['current_baller'];
					$over->six=$_SESSION['six'];
					$over->four=$_SESSION['four'];
					$over->dot=$_SESSION['dot'];
					$over->WD=$_SESSION['wd'];
					$over->NB=$_SESSION['nb'];
					$over->save();
					$_SESSION['over_outs']=0;
					$_SESSION['over_score']=0;
					$_SESSION['wd']=0;
					$_SESSION['nb']=0;
					$_SESSION['six']=0;			
					$_SESSION['four']=0;
					$_SESSION['dot']=0;	
				}
				else
				{
				///changing facing batsmen ..	
					if(($ball->score)%2 != 0)
					{

						if($_SESSION['face_by']==$_SESSION['p2'])
						{
							$_SESSION['face_by']=$_SESSION['p1'];
						}
						else
						{
							$_SESSION['face_by']=$_SESSION['p2'];

						}

					}

		/// changing facing batsmen end
					$b=$request->ball_NO;
					$ball->ball_NO=$b;
					$ball->save();
				}
				if($_SESSION['match_type']=='T20')
				{
					if($request->out=='1')
					{	

						return redirect()->route('next_player');
					}	
				}
			}
			elseif($request->validity=='WB' or $request->validity=='NB')
			{
				
				$playerscore=players::where('id',$_SESSION['face_by'])->first();
				if($request->out==1)
				{
					$playerscore->score=($playerscore->score)-($config->out_deduct);					
					$playerscore->out++;
					$_SESSION['last_ball_out']=1;
				}

				$initialscore=$playerscore->extra;
				$playerscore->extra=($initialscore)+($config->extra_score);
				if($request->score=='4')
				{
					$playerscore->four++;
				}
				if($request->score=='6')
				{
					$playerscore->six++;
				}
				/////Adding score other than extra as player's score only in case of a no ball but no in wide ball all score on wide ball is as extra
				if($request->validity=='NB')
				{
					$playerscore->score=$playerscore->score+$request->score;
					$_SESSION['last-nb-score']=$config->extra_score;

				}
				else
				{
					$playerscore->extra=$playerscore->extra+$request->score;
					$_SESSION['last-wd-score']=$config->extra_score+$request->score;
				}

				//////////////////////////////////////////

				
				$playerscore->save();
				//dd($playerscore->extra);
				$extraless=($ball->score)-1;

				if(($extraless)%2 != 0)
				{

					if($_SESSION['face_by']==$_SESSION['p2'])
					{
						$_SESSION['face_by']=$_SESSION['p2'];
					}
					else
					{
						$_SESSION['face_by']=$_SESSION['p1'];

					}

				}
				else{
					if($_SESSION['face_by']==$_SESSION['p2'])
					{
						$_SESSION['face_by']=$_SESSION['p1'];
					}
					else
					{
						$_SESSION['face_by']=$_SESSION['p2'];

					}
				}


				$ball->ball_NO=$request->ball_NO;
				$ball->score=$request->score+1;				
				$ball->save();

				if($_SESSION['match_type']=='T20')
				{
					if($request->out=='1')
					{	
						
						return redirect()->route('next_player');
					}	
				}

			}
			elseif($request->validity='BY' or $request->validity='LB')
			{

				$playerscore=players::where('id',$_SESSION['face_by'])->first();
				$playerscore->balls_played++;
				if($request->score=='4')
				{
					$playerscore->four++;
				}
				if($request->score=='6')
				{
					$playerscore->six++;
				}
				$playerscore->extra=($playerscore->extra)+($request->score);
				if($request->out==1)
				{
					$playerscore->score=($playerscore->score)-2;
					$playerscore->out++;
					$_SESSION['last_ball_out']=1;

				}
				$playerscore->save();
///////////////////////////////////////////
				if($request->ball_NO==6)
				{   
					$ball->ball_NO=$request->ball_NO;
					$ball->over_id=$request->over_id;
					$ball->save();
			///saving whole over score
					$score_sum=0;
					$out_sum=0;

					$scr=balls::where('over_id',$request->over_id)->get();
					foreach($scr as $s)
					{
						$out=$s->out;
						$out_sum=$out_sum+$out;
						$score=$s->score;
						$score_sum=$score_sum+$score;
					}
			///changing facing batsmen ..	
					if(($ball->score)%2 != 0)
					{

						if($_SESSION['face_by']==$_SESSION['p2'])
						{
							$_SESSION['face_by']=$_SESSION['p2'];
						}
						else
						{
							$_SESSION['face_by']=$_SESSION['p1'];

						}

					}
					else{
						if($_SESSION['face_by']==$_SESSION['p2'])
						{
							$_SESSION['face_by']=$_SESSION['p1'];
						}
						else
						{
							$_SESSION['face_by']=$_SESSION['p2'];

						}
					}

		/// changing facing batsmen end

					$over= new overs();
			//$over->id=$request->over_id;

					$over->team_id=$_SESSION['bat_first'];
					$over->pair_id=$_SESSION['pair'];
					$over->match_id=$_SESSION['match_id'];
					$over->score=$score_sum;
					$over->outs=$out_sum;
					$over->save();

				}
				else
				{
				///changing facing batsmen ..	
					if(($ball->score)%2 != 0)
					{

						if($_SESSION['face_by']==$_SESSION['p2'])
						{
							$_SESSION['face_by']=$_SESSION['p1'];
						}
						else
						{
							$_SESSION['face_by']=$_SESSION['p2'];

						}

					}

		/// changing facing batsmen end
					$b=$request->ball_NO;
					$ball->ball_NO=$b;
					$ball->save();
				}
				if($_SESSION['match_type']=='T20')
				{
					if($request->out=='1')
					{	
						
						return redirect()->route('next_player');
					}	
				}
			}


	//$ball->save();
			//dd($playerscore->extra);
			return redirect()->route('crichome');
		}
		public function result(){
			session_start();

			$Aplayer=players::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamA'])->get();

			$Bplayer=players::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamB'])->get();

			$Apairs=pairs::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamA'])->get();

			$Bpairs=pairs::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamB'])->get();

			$Abowlers=overs::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamA'])
			->selectRaw('count(bowled_by) as bowler_count, sum(score) as score, sum(outs) as outs,sum(dot) as dots,sum(four) as fours,sum(six) as sixs,sum(NB) as NB,sum(WD) as WD, bowled_by')
			->groupBy('bowled_by')
			->get();
			
			//dd($Abowlers);
			$Bbowlers=overs::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamB'])
			->selectRaw('count(bowled_by) as bowler_count, sum(score) as score, sum(outs) as outs,sum(dot) as dots,sum(four) as fours,sum(six) as sixs,sum(NB) as NB,sum(WD) as WD, bowled_by')
			->groupBy('bowled_by')
			->get();

			return view('result',compact('Aplayer','Bplayer','Apairs','Bpairs','Abowlers','Bbowlers'));
		}

		public function result_T20(){
			session_start();
			//dd($_SESSION['Btotal']);
			$Aplayer=players::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamA'])
			->where('pick',1)
			->get();

			$Bplayer=players::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamB'])
			->where('pick',1)
			->get();

			$Abowlers=overs::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamA'])
			->selectRaw('count(bowled_by) as bowler_count, sum(score) as score, sum(outs) as outs,sum(dot) as dots,sum(four) as fours,sum(six) as sixs,sum(NB) as NB,sum(WD) as WD, bowled_by')
			->groupBy('bowled_by')
			->get();
			
			//dd($Abowlers);
			$Bbowlers=overs::where('match_id',$_SESSION['match_id'])
			->where('team_id',$_SESSION['teamB'])
			->selectRaw('count(bowled_by) as bowler_count, sum(score) as score, sum(outs) as outs,sum(dot) as dots,sum(four) as fours,sum(six) as sixs,sum(NB) as NB,sum(WD) as WD, bowled_by')
			->groupBy('bowled_by')
			->get();
			

			return view('result_T20',compact('Aplayer','Abowlers','Bbowlers','Bplayer'));
		}



		Public function next_player()
		{
			session_start();
			$batsmen=players::where('team_id',$_SESSION['bat_first'])
			->where('pick',0)			
			->get();

			$_SESSION['face1val']=1;
			
			// dd($batsmen->count());

			if($batsmen->count()==0)
			{
				return redirect()->route('crichome');
			}


			return view('next_player',compact('batsmen'));	
		}
		public function set_new_player(Request $request)
		{
			session_start();
			if($request->gets_out==$_SESSION['p1'])
			{
				$_SESSION['p1']=$request->next_player;
			}
			elseif($request->gets_out==$_SESSION['p2'])
			{
				$_SESSION['p2']=$request->next_player;

			}
			$_SESSION['face_by']=$request->face;
			$changepick=players::where('id',$request->next_player)
			->first();
			$changepick->pick=1;
			$changepick->save();
			return redirect()->route('crichome');
		}

		public function undo()
		{
			session_start();
			$_SESSION['undo-triger']=1;
			$config=configuration::where('match_type',$_SESSION['match_type'])->first();

			$last_score_by_player=players::where('team_id',$_SESSION['bat_first'])
			->where('id',$_SESSION['last_played_by'])
			->first();
			/////////////last ball removing
			if($_SESSION['match_type']=='Indoor Advance')
			{
				//considering wide and no ball as a ball according to rules
				$last_valid_ball=balls::where('match_id',$_SESSION['match_id'])
				->orderBy('id','desc')->first();
			}
			else
			{
				//selecting row with last valid ball bowled
				$last_valid_ball=balls::where('match_id',$_SESSION['match_id'])
				->where('validity','ok')
				->orWhere('validity','=','BY')
				->orWhere('validity','=','LB')
				->orderBy('id','desc')->first();
			}
			/////////////////////////////////////////////////
			
			//score minus from player's score 
			$last_score_by_player->score=$last_score_by_player->score-$_SESSION['last_score'];

			if(isset($_SESSION['last-wd-score']))
			{
				$last_score_by_player->extra=$last_score_by_player->extra-$_SESSION['last-wd-score'];
				$last_score_by_player->score=$last_score_by_player->score+$_SESSION['last_score'];

				unset($_SESSION['last-wd-score']);
			}
			elseif(isset($_SESSION['last-nb-score']))
			{
				$last_score_by_player->extra=$last_score_by_player->extra-$_SESSION['last-nb-score'];
				unset($_SESSION['last-nb-score']);

			}
			else{
				//deleting last valid ball only when ball is not wide or extra ball
				$last_valid_ball->delete();
			}
			
			

			
			$_SESSION['total_balls']--;
			$_SESSION['face_by']=$_SESSION['last_face_by'];


			////managing out on undo
			if($_SESSION['match_type']=='T20')
			{
				
			}
			else{
				if($_SESSION['last_ball_out']==1)
				{
					
					$last_score_by_player->score=$last_score_by_player->score+$config->out_deduct;				

					$last_score_by_player->out--;
					$_SESSION['last_ball_out']=0;
					

				}
			}
			


			$last_score_by_player->save();
			return redirect()->route('crichome');
		}

	}
