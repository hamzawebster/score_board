<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class configuration extends Model
{
    protected $table="configurations";
    protected $fillables=['match_type','overs_per_pair','extra_score','out_deduct'];
}
