<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class players extends Model
{
    protected $table='players';
    protected $fillables=['name','team_id'];
}
