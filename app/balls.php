<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class balls extends Model
{
    protected $table='balls';
    protected $primarykey='id';
    protected $fillables=['over_id','ball_NO','validity','score','out'];

}
