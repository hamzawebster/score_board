<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pairs extends Model
{
    protected $table='pairs';
    protected $fillables=['p1_id','p2_id','p1_scr','p2_scr','score','team_id'];
}
