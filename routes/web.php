<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//nanana
use App\balls;
use App\overs;
use Illuminate\Http\Request;

Route::get('/','CricketController@getinfo')->name('getinfo');

Route::post('/SaveTeams','CricketController@save_teams')->name('save_teams');

Route::post('/SaveMatch','CricketController@save_match')->name('save_match');
Route::post('/AddPlayers-TeamA','CricketController@addplayers_teamA')->name('addplayers_teamA');
Route::post('/AddPlayers-TeamB','CricketController@addplayers_teamB')->name('addplayers_teamB');
Route::post('/SavePair','CricketController@save_pair')->name('save_pair');

Route::get('/ball','CricketController@ball')->name('crichome'); 


Route::post('/update','CricketController@update')->name('update');

Route::get('/restart',function(){

	return redirect()->route('getinfo');
})->name('restart');

Route::get('/Result','CricketController@result')->name('result');
Route::get('/Result_T20','CricketController@result_T20')->name('result_T20');
Route::get('/next_player','CricketController@next_player')->name('next_player');

Route::post('/set_new_player','CricketController@set_new_player')->name('set_new_player');

Route::post('/set_bowler','CricketController@set_bowler')->name('set_bowler');
Route::get('/undo','CricketController@undo')->name('undo');